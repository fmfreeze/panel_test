import panel as pn
import holoviews as hv
from holoviews.operation.datashader import datashade
import numpy as np
import param
import argparse

pn.extension()
hv.extension("bokeh")

class Action(param.Parameterized):
    """
    this is my Action class!

    Args:
        param (_type_): _description_

    Returns:
        _type_: _description_
    """
    samples: int = param.Integer(100, bounds=(3, 100000), doc="amount of samples")  # type: ignore

    @param.depends("samples", watch=True)
    def get_plot(self):
        """
        this is my plot function yeah

        Returns:
            _type_: _description_
        """
        data = np.random.randint(0, 100, self.samples)
        return datashade(hv.Curve(data))

def deploy_app():
    action = Action()
    app = pn.Column(action, action.get_plot)
    app.servable()

def print_something():
    print("okcool, this is something I always wanted to print sometime ;)")

if __name__ == "__main__":
    """
    just the main entry here!
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--deploy", help="deploy panel-bokeh webapp", action="store_true")
    args = parser.parse_args()

    if args.deploy:
        deploy_app()
    else:
        print_something()
