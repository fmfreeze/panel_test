    
import argparse
from .test import deploy_app, print_something

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--deploy", help="deploy panel-bokeh webapp", action="store_true")
    args = parser.parse_args()

    if args.deploy:
        deploy_app()
    else:
        print_something()
