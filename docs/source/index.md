# Welcome to Panel Test

This python package is developed to test the gitlab pages integration + executing a live (panel/bokeh) server app.

```{note}
This python package is under development!
```

# Content of this documenation

The following tables of content with its topic areas will help you to understand and use this package.

```{toctree}
:caption: 'Topic Areas'
:glob:
:maxdepth: 3

notebooks/Example_Notebook
core_concept
howtos
contribute
```


```{toctree}
:caption: 'References'
:glob:
:maxdepth: 3

autoapi/index
useful_links
```

- {ref}`genindex`
- {ref}`modindex`
