

:py:mod:`src.test`
==================

.. py:module:: src.test


Module Contents
---------------

.. list-table:: Classes
   :header-rows: 0
   :widths: auto
   :class: summarytable

   * - :py:obj:`Action <src.test.Action>`
     - this is my Action class!


.. list-table:: Functions
   :header-rows: 0
   :widths: auto
   :class: summarytable

   * - :py:obj:`deploy_app <src.test.deploy_app>`\ ()
     - \-



.. py:class:: Action

   Bases: :py:obj:`param.Parameterized`

   .. autoapi-inheritance-diagram:: src.test.Action
      :parts: 1

   this is my Action class!

   :param param: _description_
   :type param: _type_

   :returns: _description_
   :rtype: _type_


   .. rubric:: Overview

   .. list-table:: Attributes
      :header-rows: 0
      :widths: auto
      :class: summarytable

      * - :py:obj:`samples <src.test.Action.samples>`
        - \-


   .. list-table:: Methods
      :header-rows: 0
      :widths: auto
      :class: summarytable

      * - :py:obj:`get_plot <src.test.Action.get_plot>`\ ()
        - this is my plot function yeah


   .. rubric:: Members

   .. py:attribute:: samples
      :annotation: :int

      

   .. py:method:: get_plot()

      this is my plot function yeah

      :returns: _description_
      :rtype: _type_



.. py:function:: deploy_app()


