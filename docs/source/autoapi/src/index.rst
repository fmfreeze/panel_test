

:py:mod:`src`
=============

.. py:module:: src



.. toctree::
   :titlesonly:
   :maxdepth: 1

   test/index.rst


Package Contents
----------------

.. list-table:: Classes
   :header-rows: 0
   :widths: auto
   :class: summarytable

   * - :py:obj:`Action <src.Action>`
     - this is my Action class!




.. py:class:: Action

   Bases: :py:obj:`param.Parameterized`

   .. autoapi-inheritance-diagram:: src.Action
      :parts: 1

   this is my Action class!

   :param param: _description_
   :type param: _type_

   :returns: _description_
   :rtype: _type_


   .. rubric:: Overview

   .. list-table:: Attributes
      :header-rows: 0
      :widths: auto
      :class: summarytable

      * - :py:obj:`samples <src.Action.samples>`
        - \-


   .. list-table:: Methods
      :header-rows: 0
      :widths: auto
      :class: summarytable

      * - :py:obj:`get_plot <src.Action.get_plot>`\ ()
        - this is my plot function yeah


   .. rubric:: Members

   .. py:attribute:: samples
      :annotation: :int

      

   .. py:method:: get_plot()

      this is my plot function yeah

      :returns: _description_
      :rtype: _type_



