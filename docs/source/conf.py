# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys

sys.path.insert(0, os.path.abspath(os.path.join("..", "..")))

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

__author__ = "Franz-Martin Friess"
__email__ = "fm.friess@mcl.at"
__copyright__ = "Copyright 2022, " "Materials Center Leoben Forschung GmbH"
__license__ = "All rights reserved"
__status__ = "Development"

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

project = "panel_test"
copyright = "2023, Materials Center Leoben Forschung GmbH"
author = "Franz-Martin Friess"
release = "0.5"

extensions = [
    "autoapi.extension",  # creates automatic API references from docstrings (alternative to sphinx.autodoc)
    "myst_nb",  # makes jupyter notebooks integratable into sphinx docs + also enables myst-parser
    "sphinx.ext.napoleon",  # can parse google/numpy docstring formats
    "sphinx.ext.autodoc.typehints",
    "sphinx.ext.duration",  # measures and shows the built time for each page
    "sphinx.ext.autosectionlabel",  # makes every (sub)heading a target directive automatically
    # 'sphinx.ext.autodoc',
    # "sphinx_rtd_theme",
]

nb_execution_mode = "auto"
nb_execution_excludepatterns = ["*.ipynb"]
myst_enable_extensions = [
    # "colon_fence",
    # "deflist",
    "dollarmath",
    # "amsmath",
    # "substitution",
    # "html_image",
    # "linkify",
]

# source_suffix = {  # define which files should be parsed by which parser(-extension)
# '.ipynb': 'myst-nb'
# #'.myst': 'myst'
# }

templates_path = ["_templates"]
exclude_patterns = []

autodoc_typehints = "both"  # for typehints (extension 'sphinx.ext.autodoc.typehints' necessary) "signature" is another possible value

# autoapi_root = 'rooty'
# autoapi_ignore = []
# autoapi_keep_files = True
autoapi_type = "python"
autoapi_dirs = ["../../paneltest"]
autoapi_template_dir = "_templates/autoapi"
autoapi_options = [
    "members",
    "undoc-members",
    "show-inheritance",
    "show-module-summary",
    #'special-members',
    "imported-members",
    "show-inheritance-diagram",
]
autoapi_keep_files = True

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = "sphinx_book_theme"  # "sphinx_rtd_theme"
html_theme_options = {
    "repository_url": "https://gitlab.com/fmfreeze/panel_test",
    "use_repository_button": True,
    "use_issues_button": True,
    "use_edit_page_button": True,
    "show_toc_level": 2,
}
html_static_path = ["_static"]

# html_title = "Your title"


rst_prolog = """
.. role:: summarylabel
"""  # This custom role is for nice sphinx-autoapi summary table

html_css_files = [
    "css/custom.css",
]


def contains(seq, item):
    return item in seq


def prepare_jinja_env(jinja_env) -> None:
    jinja_env.tests["contains"] = contains


autoapi_prepare_jinja_env = prepare_jinja_env
